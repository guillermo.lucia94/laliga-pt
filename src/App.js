import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Route,
  BrowserRouter as Router,
  Routes,
  Navigate,
} from "react-router-dom";
import NavBar from "./components/NavBar";

import { userLogedIN } from "./modules/users/actions";
import { Login } from "./screens/Login";
import { UsersList } from "./screens/UsersList";

function App() {
  //--------------------------------------------------------- 🡣 Declaración de constantes.
  const dispatch = useDispatch();
  const keyUser = useSelector((state) => state.users.key);
  //--------------------------------------------------------- 🡣 Nada mas cargar la aplicacion revisamos el Store keyUser para ver si tiene valor y de tenerlo lo almacenamos en el store de Redux.
  useEffect(() => {
    const userLogged = localStorage.getItem("keyUser");
    if (userLogged) {
      dispatch(userLogedIN(userLogged));
    }
  }, []);
  //--------------------------------------------------------- 🡣 Control de KeyUser, con ello sabemos si esta loggeado el usuario y a donde redirigirle en caso de que intente acceder a otra dirección.
  const controlUserLogg = () => {
    if (keyUser) {
      return (
        <>
          <Route path="/users" element={<UsersList />} />
          <Route path="*" element={<Navigate replace to="/users" />} />
        </>
      );
    } else {
      return (
        <>
          <Route path="/login" element={<Login />} />
          <Route path="*" element={<Navigate replace to="/login" />} />
        </>
      );
    }
  };
  //--------------------------------------------------------- 🡣 RETURN DE APP.JS
  return (
    <Router>
      {/* 🡣 Navegador de la web. */}
      <NavBar />
      <Routes>{controlUserLogg()}</Routes>
    </Router>
  );
}

export default App;
