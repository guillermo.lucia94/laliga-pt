import { Button, Input } from "antd";
import React, { useState } from "react";
import {
  Div_Login,
  Div_Login_Container,
  Login_ButtonContainer_Size,
  Login_ErrorMContainer_Size,
  Login_InputsContainer_Size,
  Login_TitleContainer_Size,
} from "../styles/Login.style";
import { useDispatch, useSelector } from "react-redux";

import { llamadaAPILogIN } from "../modules/users/actions";

export const BoxLogIn = () => {
  //--------------------------------------------------------- 🡣 Declaración constantes.
  const [passwordValue, setPasswordValue] = useState(null);
  const [userValue, setUserValue] = useState(null);
  const dispatch = useDispatch();
  const mensajeError = useSelector((state) => state.users.mensajeErrorLogIn);

  const llamadaAPiLogin = (user, pass) => {
    dispatch(llamadaAPILogIN(user, pass)); //---> LLamada al actions de Thunk que gestiona el LoggIN verificando la password y el mail
  };

  return (
    <>
      <Div_Login_Container>
        <Div_Login>
          <Login_TitleContainer_Size>
            <h4>Se requiere autenticación</h4>
            <hr />
          </Login_TitleContainer_Size>
          <Login_InputsContainer_Size>
            <Input
              placeholder="ejemplousuario@gmail.com"
              onChange={(e) => {
                setUserValue(e.target.value);
              }}
            />
            <br />
            <br />
            <Input.Password
              placeholder="Contraseña"
              onChange={(e) => {
                setPasswordValue(e.target.value);
              }}
            />
            <Login_ErrorMContainer_Size>
              {/* 🡣 Si existe algun error en el LogIn este se almacena en Redux y automáticamente lo recibimos y mostramos al usuario de forma dinamica en función del error */}
              {mensajeError ? mensajeError : ""}
            </Login_ErrorMContainer_Size>
          </Login_InputsContainer_Size>
          <Login_ButtonContainer_Size>
            <Button
              type="primary"
              onClick={() => {
                llamadaAPiLogin(userValue, passwordValue);
              }}
            >
              Iniciar Sesión
            </Button>
          </Login_ButtonContainer_Size>
        </Div_Login>
      </Div_Login_Container>
    </>
  );
};
