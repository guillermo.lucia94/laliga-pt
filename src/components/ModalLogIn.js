import React, { useEffect, useState } from "react";
import { Modal, Button, Input } from "antd";
import "antd/dist/antd.css";
import { useDispatch } from "react-redux";
import { llamadaAPILogIN } from "../modules/users/actions";

export const ModalLogIn = () => {
  //--------------------------------------------------------- 🡣 Declaración constantes.
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [passwordValue, setPasswordValue] = useState(null);
  const [userValue, setUserValue] = useState(null);
  const dispatch = useDispatch();

  const handleOk = () => {
    llamadaAPiLogin(userValue, passwordValue);
  };

  const handleCancel = async () => {
    setIsModalVisible(false);
  };

  const llamadaAPiLogin = (user, pass) => {
    dispatch(llamadaAPILogIN(user, pass)); //---> LLamada al actions de Thunk que gestiona el LoggIN verificando la password y el mail
  };

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Login Pop-up
      </Button>
      <Modal
        title="Login Pop-up"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Iniciar Sesión"
        cancelText="Cancelar"
      >
        <Input
          placeholder="ejemplousuario@gmail.com"
          onChange={(e) => {
            setUserValue(e.target.value);
          }}
        />
        <br />
        <br />
        <Input.Password
          placeholder="Contraseña"
          onChange={(e) => {
            setPasswordValue(e.target.value);
          }}
        />
      </Modal>
    </>
  );
};
