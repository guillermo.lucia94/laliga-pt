import React, { useState } from "react";
import { Modal, Button } from "antd";
import "antd/dist/antd.css";
import { useDispatch } from "react-redux";
import { userLogedOff } from "../modules/users/actions";

export const ModalLogOff = () => {
  //--------------------------------------------------------- 🡣 Declaración constantes.
  const [isModalVisible, setIsModalVisible] = useState(false);
  const dispatch = useDispatch();

  const handleOk = () => {
    setIsModalVisible(false);
    logOFF();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const logOFF = () => {
    dispatch(userLogedOff()); //------> Limpiamos estado de Redux asociado al usuario y vaciamos el KeyUser del LocalStorage
  };

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Cerrar Sesión
      </Button>
      <Modal
        title="¿Desea cerrar la sesión?"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Cerrar sesión"
        cancelText="Cancelar"
      >
        Le recordamos que una vez haya cerrado la sesión no tendra acceso a la
        lista de Users.
      </Modal>
    </>
  );
};
