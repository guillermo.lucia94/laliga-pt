import { NavLink } from "react-router-dom";
import React from "react";
import { useSelector } from "react-redux";
import { ModalLogOff } from "./ModalLogOff";
import { HeaderImg, NavbarContainer } from "../styles/Navbar.style";
import { ModalLogIn } from "./ModalLogIn";

export default function NavBar() {
  const data = useSelector((state) => state.users.key);
  //--------------------------------------------------------- 🡣 Controlamos si el usuario ha iniciado sesión, en funcion a eso mostramos un fast Login o un Logoff (cerrar sesión) en la cabecera de la web.
  const usersShow = () => {
    if (data) {
      return (
        <>
          {/* 🡣 Modal de antd para confirmar cerrar la sesión. */}
          <ModalLogOff></ModalLogOff>
        </>
      );
    } else {
      return (
        <>
          {/* 🡣 Modal de antd para sesión de forma rápida. */}
          <ModalLogIn></ModalLogIn>
        </>
      );
    }
  };

  return (
    <div>
      <NavbarContainer>
        <HeaderImg></HeaderImg>
        {usersShow()}
      </NavbarContainer>
    </div>
  );
}
