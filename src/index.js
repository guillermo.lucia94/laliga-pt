import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import createStore from "./modules/createStore";
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={createStore()}>
    <App />
  </Provider>,

  document.getElementById("root")
);

reportWebVitals();
