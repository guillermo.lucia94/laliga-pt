import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";
import thunkMiddleware from "redux-thunk";

import rootReducer from "./rootReducer";

const middlewares = [thunkMiddleware]; //Mantenemos en un array los middlewares con proyeccion a escalar el proyecto y poder utilizar tanto thunk como sagas. Ejemplo: const middlewares = [tokenMiddleware, sagaMiddleware, thunkMiddleware];

export default (initialState) => {
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
  );
  return store;
};
