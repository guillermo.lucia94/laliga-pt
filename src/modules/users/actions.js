import {
  GET_USERS,
  LLAMADA_API_LOGIN,
  MESSAGE_ERROR_LOGIN,
} from "./constants.js";
import { message } from "antd";
import axios from "axios";

//--------------------------------------------------------- 🡣 Declaración de getUsers, realiza una llamada dinamica pasandole la pagina en la URL "pageID".

export const getUsers = (pageID) => async (dispatch) => {
  await axios
    .get(`https://reqres.in/api/users?page=${pageID}`)
    .then((response) => {
      //-----------  🡣 Lanzamos una acción de tipo GET_USERS que le pasa el valor del response generado de la llamada API, esto informa getUsersList
      dispatch({
        type: GET_USERS,
        payload: response.data.data,
      });
    })
    .catch((error) => {
      console.log("error en llamada API", error); //-- Gestion de errores por consola, en esta llamada no se necesita informar al usuario del error.
    });
};

//--------------------------------------------------------- 🡣 Declaración de llamadaAPILogIN, realiza una llamada dinamica a la API pasando un usuario y una password (user, pass).

export const llamadaAPILogIN = (user, pass) => async (dispatch) => {
  let txtMensajeError = false; //---> Posibles errores que puedan surgir del LogIn
  //-----------  🡣 Necesario generar un Body que se le pasa como segundo parametro a la url de la API
  let body = {
    email: user,
    password: pass,
  };

  if (pass === "cityslicka") {
    //*********** IMPORTANTE: reqres no controla la contraseña que se le pasa solo verifica el usuario, en la prueba se solicita control de ambos campos por eso se añade un if previo     *********** */
    await axios
      .post("https://reqres.in/api/login", body)
      .then((response) => {
        dispatch({
          type: LLAMADA_API_LOGIN,
          payload: response.data.token,
        });
        txtMensajeError = false;
        localStorage.setItem("keyUser", response.data.token); //--> Si el logIn es correcto informamos en el localStorage la API KEY.
      })
      .catch((error) => {
        //----> Gestion de errores en el LogIn
        console.log("mensaje de error llamada API traspasos: ", error); //----> Error tecnico para el desarrollo.
        txtMensajeError =
          "El email introducido no corresponde con ningun usuario registrado."; //---> Mensaje de error de control para el usuario
      });
  } else {
    if (user) {
      txtMensajeError =
        "Password incorrecta, se tienen en cuenta las mayusculas."; //---> Mensaje de error de control para el usuario
    } else {
      txtMensajeError = "Rellene todos los datos para poder iniciar sesión."; //---> Mensaje de error de control para el usuario
    }
  }
  //-----------  🡣 Lanzamos una acción de tipo MESSAGE_ERROR_LOGIN, se le pasa siempre el valor de txtMensajeError que al iniciar llamadaAPILogIN es false , si hay algun error se informa y se muestra al usuario de forma amigable y no solo por consola.
  dispatch({
    type: MESSAGE_ERROR_LOGIN,
    payload: txtMensajeError,
  });
  txtMensajeError
    ? message.error("Se ha producido un error al iniciar sesión.") //----> Notificacion estilo popup con el resultado del LogIn
    : message.success("Sesión iniciada correctamente."); //----> Notificacion estilo popup con el resultado del LogIn
};

//--------------------------------------------------------- 🡣 Declaración de userLogedOff, damos de baja el usuario tanto en el estadod e Redux como en el LocalStorage.
export const userLogedOff = () => async (dispatch) => {
  await dispatch({
    type: LLAMADA_API_LOGIN,
    payload: false,
  });
  localStorage.setItem("keyUser", ""); //-----> Vaciamos el localStorage.
};

//--------------------------------------------------------- 🡣 Declaración de userLogedIN, sirve de control al iniciar la Aplicacion si hay algun KeyUser informado este se almacena en una estado Redux, luego trabajamos con ese estado y no nos preocupamos del localStorage (lo suyo seria encriptar el localStorage).
export const userLogedIN = (key) => async (dispatch) => {
  await dispatch({
    type: LLAMADA_API_LOGIN,
    payload: key,
  });
};
