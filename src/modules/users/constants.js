export const GET_USERS = "GET_ALL_USERS"; //---> Almacenamiento de Usuarios
export const LLAMADA_API_LOGIN = "LLAMADA_API_LOGIN"; //----> LogIn del Cliente
export const MESSAGE_ERROR_LOGIN = "MESSAGE_ERROR_LOGIN"; //---> Mensaje de error que se le muestra al cliente en el LogIn
