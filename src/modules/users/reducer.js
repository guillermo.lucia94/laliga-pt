import {
  GET_USERS,
  LLAMADA_API_LOGIN,
  MESSAGE_ERROR_LOGIN,
} from "./constants.js";

export const INITIAL_STATE = {
  getUsersList: [],
  key: false,
  mensajeErrorLogIn: false,
};
//---------------------------------------------- Gestión de tipo de estado y valor de estados en redux.
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        getUsersList: action.payload,
      };
    case LLAMADA_API_LOGIN:
      return {
        ...state,
        key: action.payload,
      };
    case MESSAGE_ERROR_LOGIN:
      return {
        ...state,
        mensajeErrorLogIn: action.payload,
      };
    default:
      return state;
  }
};
