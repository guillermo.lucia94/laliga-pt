import React, { useEffect, useState } from "react";
import { getUsers } from "../modules/users/actions";
import { useDispatch, useSelector } from "react-redux";
import {
  Div_Avatar_Container,
  Div_Pagination_Container,
  Div_Users_Container,
} from "../styles/Users.style";
import { Card, Pagination } from "antd";
import Meta from "antd/lib/card/Meta";

export const UsersList = () => {
  //--------------------------------------------------------- 🡣 Declaración de constantes.
  const [pageUsers, setpageUsers] = useState(1); //-- Ponemos por defecto el 1 para que acceda en la primera lectura a la pagina 1.
  const dispatch = useDispatch();
  const usersData = useSelector((state) => state.users.getUsersList);
  //--------------------------------------------------------- 🡣 useEffect carga inicial de Users con una escucha al useState pageUsers, realiza una llamada dinamica a la API en función del paginado.
  useEffect(() => {
    getUsersData();
  }, [pageUsers]);

  const getUsersData = () => {
    dispatch(getUsers(pageUsers)); //----- LLamada al actions de thunk getUsers
  };
  //--------------------------------------------------------- 🡣 Gestión de paginado con useState.
  const handlePagination = (e) => {
    setpageUsers(e);
  };
  //--------------------------------------------------------- 🡣 Recorrido de usersData preparando card de forma dinamica de los usuarios (las cards estan sacadas de antd).
  const fichaUsuario = usersData.map((obj) => {
    return (
      <span key={obj.id}>
        <Div_Avatar_Container>
          <Card
            size="small"
            hoverable
            style={{ width: 140 }}
            cover={<img src={obj.avatar} />}
          >
            <Meta
              title={obj.first_name + " " + obj.last_name}
              description={obj.email}
            />
          </Card>
        </Div_Avatar_Container>
      </span>
    );
  });
  //--------------------------------------------------------- 🡣 RETURN DE USERSLIST.JS
  return (
    <>
      {/* 🡣  Se LLama a fichaUsuario para generar los perfiles */}
      <Div_Users_Container>{fichaUsuario}</Div_Users_Container>
      <Div_Pagination_Container>
        <Pagination
          defaultCurrent={pageUsers} //-----> Inicio del paginado en la posicion 1 (default de pageUsers).
          total={12} //------> se le indica que el total de usuarios es de 12 (esta parte se podria poner de forma dinamica sacando la longitud de getUsers y pasandoselo por useState en este caso no ha sido necesario).
          pageSize={6} //------> se limita el tamaño de la pagina a 6 usuarios.
          onChange={(e) => {
            //----> Gestiona el cambio de pagina.
            handlePagination(e);
          }}
        />
      </Div_Pagination_Container>
    </>
  );
};
