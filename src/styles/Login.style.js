import styled from "styled-components";

export const Div_Login_Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 15vh;
`;

export const Div_Login = styled.div`
  text-align: center;
  width: 20vw;
  min-width: 260px;
  height: 300px;
  padding: 10px;
  box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px,
    rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset;
`;

export const Login_TitleContainer_Size = styled.div`
  height: 20%;
`;
export const Login_InputsContainer_Size = styled.div`
  height: 60%;
`;
export const Login_ButtonContainer_Size = styled.div`
  height: 20%;
`;
export const Login_ErrorMContainer_Size = styled.div`
  padding-top: 20px;
  color: red;
`;
