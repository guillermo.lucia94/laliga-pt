import styled from "styled-components";
import logo_home from "../images/logo_home.png";

export const NavbarContainer = styled.nav`
  margin: auto;
  width: 100%;
  height: 60px;
  padding: 3px;
  padding-left: 4vw;
  padding-right: 2vw;
  padding-top: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  line-height: 30px;
  box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
`;

export const HeaderImg = styled.img.attrs({
  src: `${logo_home}`,
})`
  margin-top: -12px;
  width: 53px;
  height: 53px;
`;
