import styled from "styled-components";

export const Div_Users_Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10vh;
  flex-direction: row;
  justify-content: space-evenly;
  @media (max-width: 667px) {
    flex-direction: column;
    margin-top: 0px;
  }
`;

export const Div_Avatar_Container = styled.div`
  margin-top: 20px;
`;

export const Div_Pagination_Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 5vh;
`;
